{OPENAGHDEFINITION( name="Funkcja okresowa")}
Funkcję {OPENAGHMATHJAX()}f:X\to\mathbb R{OPENAGHMATHJAX} nazywamy okresową, jeśli istnieje taka liczba {OPENAGHMATHJAX()}w\neq 0{OPENAGHMATHJAX}, że dla każdego {OPENAGHMATHJAX()}x\in X{OPENAGHMATHJAX} zachodzą warunki {OPENAGHMATHJAX()}x\pm w\in X{OPENAGHMATHJAX} oraz {OPENAGHMATHJAX()}f(x\pm w)=f(x){OPENAGHMATHJAX}.
Liczbę {OPENAGHMATHJAX()}w{OPENAGHMATHJAX} nazywamy __okresem funkcji__. Jeżeli istnieje najmniejszy __dodatni__ okres, to nazywamy go __okresem podstawowym__.{OPENAGHDEFINITION}

{img type="fileId" fileId="206"}

{OPENAGHEXAMPLE( name="")}
Okresem funkcji {OPENAGHMATHJAX()}f(x)=\sin x{OPENAGHMATHJAX}  jest na przykład liczba {OPENAGHMATHJAX()}4\pi {OPENAGHMATHJAX}. Jej okresem podstawowym jest liczba {OPENAGHMATHJAX()}w=2\pi{OPENAGHMATHJAX}{OPENAGHEXAMPLE} 

{OPENAGHEXAMPLE( name="")}
Funkcja stała {OPENAGHMATHJAX()}f(x)=c{OPENAGHMATHJAX} jest funkcją okresową, ale nie ma okresu podstawowego, bo każda liczba dodatnia może być jej okresem.{OPENAGHEXAMPLE}
 

{OPENAGHANNOTATION( name="")}
Jeżeli funkcja {OPENAGHMATHJAX()}x\mapsto f(x){OPENAGHMATHJAX} jest funkcja okresową o okresie {OPENAGHMATHJAX()}w{OPENAGHMATHJAX}, zaś {OPENAGHMATHJAX()}a\in\mathbb R\setminus \{0\}{OPENAGHMATHJAX}, to funkcja {OPENAGHMATHJAX()}x\mapsto f(x)+a{OPENAGHMATHJAX} oraz funkcja {OPENAGHMATHJAX()}x\mapsto af(x){OPENAGHMATHJAX} mają ten sam okres, natomiast funkcja {OPENAGHMATHJAX()}x\mapsto f(ax){OPENAGHMATHJAX} ma okres {OPENAGHMATHJAX()}{w\over {\vert a\vert}}{OPENAGHMATHJAX}{OPENAGHANNOTATION}


Aby sporządzić wykres funkcji okresowej, wystarczy narysować go dla argumentów z dowolnego przedziału o długości {OPENAGHMATHJAX()}w{OPENAGHMATHJAX}, a następnie „powielić” na prawo i lewo od tego przedziału. Podobnie, aby podać funkcję okresową wystarczy zadać jej wartości w takim przedziale. Najbardziej znanymi funkcjami okresowymi są funkcje trygonometryczne. Okres podstawowy funkcji sinus i cosinus wynosi {OPENAGHMATHJAX()}2\pi{OPENAGHMATHJAX}, zaś funkcji tangens i cotangens {OPENAGHMATHJAX()}\pi{OPENAGHMATHJAX}.

{openaghexercise name="" body="Które z funkcji {OPENAGHMATHJAX()}f{OPENAGHMATHJAX}, {OPENAGHMATHJAX()}g{OPENAGHMATHJAX}, {OPENAGHMATHJAX()}h{OPENAGHMATHJAX} są okresowe ? Wskaż, o ile istnieją,  ich okresy podstawowe.
# {OPENAGHMATHJAX()}f:\mathbb R_-\to \mathbb R\quad x\to\sin x{OPENAGHMATHJAX},
# {OPENAGHMATHJAX()}g:\quad x\mapsto \cos 8x{OPENAGHMATHJAX}, 
# {OPENAGHMATHJAX()}h:\quad x\mapsto \vert \sin {1\over 2}x\vert {OPENAGHMATHJAX}. " solution="Funkcja {OPENAGHMATHJAX()}f{OPENAGHMATHJAX} ma z góry zadany zbiór określoności, natomiast funkcje {OPENAGHMATHJAX()}g{OPENAGHMATHJAX}, {OPENAGHMATHJAX()}h{OPENAGHMATHJAX} będziemy rozpatrywać w ich dziedzinach naturalnych.
__Ad 1.__
Funkcja {OPENAGHMATHJAX()}f{OPENAGHMATHJAX} nie jest funkcja okresową, gdyż nie spełnia pierwszego warunku definicyjnego dotyczącego dziedziny. Jakkolwiek próbowalibyśmy dobrać okres {OPENAGHMATHJAX()}w>0{OPENAGHMATHJAX}, to znajdziemy takie {OPENAGHMATHJAX()}x\in \mathbb R_-{OPENAGHMATHJAX}, że {OPENAGHMATHJAX()}x+w\notin \mathbb R_-{OPENAGHMATHJAX}.
uwaga: gdyby funkcja ta była podana następująco {OPENAGHMATHJAX()}x\mapsto \sin x{OPENAGHMATHJAX} to rozpatrywalibyśmy ją w jej dziedzinie naturalnej, (tzn. {OPENAGHMATHJAX()}\mathbb R{OPENAGHMATHJAX} i oczywiście stwierdzilibyśmy, że jest to funkcja okresowa o okresie zasadniczym {OPENAGHMATHJAX()}2\pi{OPENAGHMATHJAX}).

__Ad 2.__

Dziedziną naturalną funkcji {OPENAGHMATHJAX()}g{OPENAGHMATHJAX} jest {OPENAGHMATHJAX()}\mathbb R{OPENAGHMATHJAX}, gdyż wyrażenie {OPENAGHMATHJAX()}\cos 8x{OPENAGHMATHJAX} ma sens dla dowolnej liczby rzeczywistej, więc pierwszy warunek definicyjny jest spełniony dla dowolnego {OPENAGHMATHJAX()}w>0{OPENAGHMATHJAX}. W związku z postacią funkcji {OPENAGHMATHJAX()}g{OPENAGHMATHJAX} (pamiętając, że funkcja {OPENAGHMATHJAX()}x\mapsto\cos x{OPENAGHMATHJAX} jest funkcją okresową o okresie zasadniczym {OPENAGHMATHJAX()}2\pi{OPENAGHMATHJAX}) stwierdzamy, że funkcja {OPENAGHMATHJAX()}g{OPENAGHMATHJAX} też jest okresowa. Jej okres zasadniczy wynosi {OPENAGHMATHJAX()}{{2\pi}\over 8}={\pi\over 4}{OPENAGHMATHJAX}.

__Ad 3.__

Podobnie jak dla funkcji {OPENAGHMATHJAX()}g{OPENAGHMATHJAX} pierwszy warunek definicyjny jest spełniony. Dalszą część zadania rozwiążemy graficznie, korzystając z uwag dotyczących rysowania wykresów funkcji. Rysujemy etapami wykresy:{OPENAGHMATHJAX()}x\mapsto\sin x{OPENAGHMATHJAX}, {OPENAGHMATHJAX()}x\mapsto\sin{1\over 2}x{OPENAGHMATHJAX},{OPENAGHMATHJAX()}x\mapsto \vert\sin{1\over 2}x\vert.{OPENAGHMATHJAX}

{img type="fileId" fileId="560"}
 " anchor="Tu wpisz etykietę"}


{OPENAGHANNOTATION( name="")}Funkcje okresowe znajdują zastosowanie w technice do opisu zjawisk cyklicznych, np. drgań mechanicznych i akustycznych.{OPENAGHANNOTATION}

{OPENAGHDEFINITION( name="Parzystość i nieparzystość funkcji")}__Funkcję__ {OPENAGHMATHJAX()}f:X\to Y{OPENAGHMATHJAX} nazywamy __parzystą__ wtedy i tylko wtedy, gdy dla każdego {OPENAGHMATHJAX()}x\in X{OPENAGHMATHJAX} liczba {OPENAGHMATHJAX()}(-x)\in X{OPENAGHMATHJAX} oraz {OPENAGHMATHJAX()}f(-x)=f(x){OPENAGHMATHJAX}. __Funkcję__ {OPENAGHMATHJAX()}f:X\to Y{OPENAGHMATHJAX} nazywamy __nieparzystą__ wtedy i tylko wtedy, gdy dla każdego {OPENAGHMATHJAX()}x\in X{OPENAGHMATHJAX} liczba {OPENAGHMATHJAX()}(-x)\in X{OPENAGHMATHJAX} oraz {OPENAGHMATHJAX()}f(-x)=-f(x){OPENAGHMATHJAX}.{OPENAGHDEFINITION}

{img type="fileId" fileId="561"}
 

{img type="fileId" fileId="562"}
 

{OPENAGHANNOTATION( name="")}Warunek pierwszy wspólny dla funkcji parzystej i nieparzystej oznacza, że dziedzina każdej z nich powinna być symetryczna względem {OPENAGHMATHJAX()}(0, 0){OPENAGHMATHJAX}. W szczególności gdy {OPENAGHMATHJAX()}X=\mathbb R{OPENAGHMATHJAX}, jest on trywialnie spełniony. Wykres funkcji parzystej jest symetryczny względem osi {OPENAGHMATHJAX()}0y{OPENAGHMATHJAX}, a nieparzystej względem początku układu współrzędnych czyli  punktu {OPENAGHMATHJAX()}(0, 0){OPENAGHMATHJAX}.{OPENAGHANNOTATION}

{OPENAGHANNOTATION( name="")}Spośród czterech podstawowych funkcji trygonometrycznych jedynie funkcja {OPENAGHMATHJAX()}x\mapsto\cos x{OPENAGHMATHJAX} jest parzysta, pozostałe są nieparzyste. Inne przykładowe funkcje parzyste, to {OPENAGHMATHJAX()}x\mapsto x^2{OPENAGHMATHJAX}, {OPENAGHMATHJAX()}x\mapsto  x^4{OPENAGHMATHJAX}, {OPENAGHMATHJAX()}x\mapsto \vert x\vert {OPENAGHMATHJAX}, {OPENAGHMATHJAX()}x\mapsto \sin^2x{OPENAGHMATHJAX}, a nieparzyste {OPENAGHMATHJAX()}x \mapsto x^3{OPENAGHMATHJAX}, {OPENAGHMATHJAX()}x\mapsto x^5{OPENAGHMATHJAX}. Zauważmy, że większość funkcji nie ma ani własności parzystości, ani nieparzystości.{OPENAGHANNOTATION}


{openaghexercise name="" body="Zbadajmy parzystość i nieparzystość funkcji: {OPENAGHMATHJAX()}f: x\mapsto\log_3 {{5+x}\over {5-x}} {OPENAGHMATHJAX}." solution="{OPENAGHMATHJAX()}D_f=\{x:5-x\neq 0 \quad \wedge \quad {{5+x}\over {5-x}}>0\}.{OPENAGHMATHJAX} Rozwiązując nierówność{OPENAGHMATHJAX()}{{5+x}\over {5-x}}>0{OPENAGHMATHJAX} mamy {OPENAGHMATHJAX()}x\in (-5,5){OPENAGHMATHJAX}.

Stąd {OPENAGHMATHJAX()}D_f=(-5,5){OPENAGHMATHJAX}. Jest to oczywiście przedział symetryczny względem punktu {OPENAGHMATHJAX()}(0, 0){OPENAGHMATHJAX}, czyli warunek pierwszy jest spełniony. Obliczmy {OPENAGHMATHJAX()}f(-x){OPENAGHMATHJAX}.
{OPENAGHMATHJAX()}f(-x)=\log_3{{5-x}\over {5+x}}=\log_3\left({{5+x}\over {5-x}}\right)^{-1}=-\log_3{{5+x}\over {5-x}}=-f(x).{OPENAGHMATHJAX}

__Odpowiedź__
Funkcja {OPENAGHMATHJAX()}f{OPENAGHMATHJAX} jest nieparzysta." anchor="Tu wpisz etykietę"}


{openaghexercise name="" body="Zbadajmy parzystość i nieparzystość funkcji: {OPENAGHMATHJAX()}g: x\mapsto\sin(\cos 2x)+\vert\sin x\vert+4{OPENAGHMATHJAX}" solution=" {OPENAGHMATHJAX()}D_f=\mathbb R{OPENAGHMATHJAX}, więc warunek dotyczący symetrii dziedziny jest spełniony. Obliczając {OPENAGHMATHJAX()}g(-x){OPENAGHMATHJAX} mamy
{OPENAGHMATHJAX()}g(-x)=\sin (\cos(-2x))+\vert\sin (-x)\vert+4=\sin (\cos(-2x))+\vert-\sin x\vert+4=\sin (\cos 2x)+\vert\sin x\vert +4=g(x).{OPENAGHMATHJAX}

__Odpowiedź__
Funkcja {OPENAGHMATHJAX()}g{OPENAGHMATHJAX} jest parzysta."}


{openaghtheorem name="O rozkładzie funkcji na parzystą i nieparzystą" thesis="
Każdą funkcję {OPENAGHMATHJAX()}f{OPENAGHMATHJAX} o dziedzinie symetrycznej względem punktu {OPENAGHMATHJAX()}(0, 0){OPENAGHMATHJAX} można przedstawić w postaci sumy dwoch funkcji {OPENAGHMATHJAX()}f_1 i f_2{OPENAGHMATHJAX}, z których pierwsza jest parzysta, a druga nieparzysta.

Wówczas 
{OPENAGHMATHJAX( type="block")}f_1(x)={{f(x)+f(-x)}\over 2}{OPENAGHMATHJAX},{OPENAGHMATHJAX( type="block")}f_2(x)={{f(x)-f(-x)}\over 2}{OPENAGHMATHJAX}."}

{OPENAGHEXAMPLE( name="")}Rozłożymy funkcję {OPENAGHMATHJAX()}f(x)=3x^2+2x+7{OPENAGHMATHJAX} na sumy części parzystej i nieparzystej.

__Rozwiązanie__ 
{OPENAGHMATHJAX()}D_f=\mathbb R{OPENAGHMATHJAX}  jest zbiorem symetrycznym względem punktu {OPENAGHMATHJAX()}(0, 0){OPENAGHMATHJAX}, czyli taki rozkład jest możliwy.{OPENAGHMATHJAX()}f_1(x)={{f(x)+f(-x)}\over 2}={{3x^2+2x+7+(3(-x)^2+2(-x)+7}\over 2}={{6x^2+14}\over 2}=3x^2+7{OPENAGHMATHJAX},{OPENAGHMATHJAX()}f_2(x)={{f(x)-f(-x)}\over 2}={{3x^2+2x+7-(3(-x)^2-2x+7)}\over 2}={{3x^2+2x+7-3x^2+2x-7}\over 2}={4x\over 2}=2x{OPENAGHMATHJAX}.

__Odpowiedź__
Część parzysta funkcji {OPENAGHMATHJAX()}f{OPENAGHMATHJAX} to funkcja kwadratowa {OPENAGHMATHJAX()}f_1(x)=3x^2+7{OPENAGHMATHJAX}, część nieparzysta funkcji {OPENAGHMATHJAX()}f{OPENAGHMATHJAX} to funkcja liniowa {OPENAGHMATHJAX()}f_2(x)=2x{OPENAGHMATHJAX}{OPENAGHEXAMPLE}


{OPENAGHEXAMPLE( name="")}Znajdziemy część parzystą i część nieparzystą funkcji {OPENAGHMATHJAX()}f(x)=2\sin (6x){OPENAGHMATHJAX}.

__Rozwiązanie__
{OPENAGHMATHJAX()}D_f=\mathbb R{OPENAGHMATHJAX}  jest zbiorem symetrycznym względem {OPENAGHMATHJAX()}(0, 0){OPENAGHMATHJAX}, czyli taki rozkład jest możliwy.{OPENAGHMATHJAX()}f_1(x)={{f(x)+f(-x)}\over 2}={{2\sin 6x+2\sin (-6x)}\over 2}={{2\sin 6x-2\sin 6x}\over 2}={0\over 2}=0{OPENAGHMATHJAX},{OPENAGHMATHJAX()}f_2(x)={{f(x)-f(-x)}\over 2}={{2\sin 6x-2\sin (-6x)}\over 2}={{2\sin 6x+2\sin 6x}\over 2}=2\sin 6x{OPENAGHMATHJAX}.
Zauważmy, że tu {OPENAGHMATHJAX()}f_2=f{OPENAGHMATHJAX}. Wynika to z faktu, że dana funkcja {OPENAGHMATHJAX()}f{OPENAGHMATHJAX} jest funkcją nieparzystą. Wówczas jej częścią parzystą jest funkcja tożsamościowo równa zeru.
__Odpowiedź__
Część parzysta funkcji f to funkcja {OPENAGHMATHJAX()}f_1(x)=0{OPENAGHMATHJAX}, część nieparzysta funkcji {OPENAGHMATHJAX()}f{OPENAGHMATHJAX} to funkcja {OPENAGHMATHJAX()}f_2(x)=2\sin 6 x{OPENAGHMATHJAX}.{OPENAGHEXAMPLE}
 

 

{OPENAGHDEFINITION( name="Funkcja ograniczona z góry")}Funkcja {OPENAGHMATHJAX()}f:X\to Y{OPENAGHMATHJAX} jest ograniczona z góry, jeżeli jej zbiór wartości jest __ograniczony z góry__, czyli jeśli istnieje taka liczba {OPENAGHMATHJAX()}M{OPENAGHMATHJAX}, że dla każdego {OPENAGHMATHJAX()}x{OPENAGHMATHJAX} należacego do dziedizny funkcji {OPENAGHMATHJAX()}f(x)\le M.{OPENAGHMATHJAX}{OPENAGHDEFINITION}


{img type="fileId" fileId="564"}
 

{OPENAGHEXAMPLE( name="")}Funkcja {OPENAGHMATHJAX()}f(x)=3-\vert x\vert{OPENAGHMATHJAX} jest ograniczona z góry. Jako {OPENAGHMATHJAX()}M{OPENAGHMATHJAX} można przyjąć liczbę {OPENAGHMATHJAX()}3{OPENAGHMATHJAX} lub każdą liczbę większą od {OPENAGHMATHJAX()}3{OPENAGHMATHJAX}. Nierówność {OPENAGHMATHJAX()}f(x)\le M{OPENAGHMATHJAX} przyjmuje tu postać {OPENAGHMATHJAX()}3-\vert x\vert\le 3{OPENAGHMATHJAX} równoważną nierówności {OPENAGHMATHJAX()}\vert x\vert\ge 0{OPENAGHMATHJAX}, która jest zawsze spełniona.{OPENAGHEXAMPLE} 

{OPENAGHDEFINITION( name="Funkcja ograniczona z dołu")}Funkcja {OPENAGHMATHJAX()}f:X\to Y{OPENAGHMATHJAX} jest ograniczona z dołu, jeżeli jej zbiór wartości jest __ograniczony z dołu__, czyli jeśli istnieje taka liczba __{OPENAGHMATHJAX()}m{OPENAGHMATHJAX}__, że dla każdego {OPENAGHMATHJAX()}x\in D_f{OPENAGHMATHJAX} zachodzi {OPENAGHMATHJAX()}f(x)\ge m.{OPENAGHMATHJAX}{OPENAGHDEFINITION}


{img type="fileId" fileId="565"}
 

{OPENAGHEXAMPLE( name="")}Funkcja {OPENAGHMATHJAX()}f(x)=2^x-1{OPENAGHMATHJAX} jest ograniczona z dołu przez liczbę {OPENAGHMATHJAX()}-1{OPENAGHMATHJAX}. Nierówność {OPENAGHMATHJAX()}f(x)\ge m{OPENAGHMATHJAX} przyjmuje tu postać {OPENAGHMATHJAX()}2^x-1\ge -1{OPENAGHMATHJAX}, czyli {OPENAGHMATHJAX()}2^x\ge 0{OPENAGHMATHJAX}, co jest prawdą dla każdego {OPENAGHMATHJAX()}x\in\mathbb R.{OPENAGHMATHJAX}.{OPENAGHEXAMPLE}

{OPENAGHEXAMPLE( name="")}Funkcja {OPENAGHMATHJAX()}f(x)=x^3{OPENAGHMATHJAX} nie jest ograniczona ani z dołu, ani z góry, bo dla dowolnie dużego {OPENAGHMATHJAX()}M{OPENAGHMATHJAX} można wskazać takie {OPENAGHMATHJAX()}x{OPENAGHMATHJAX}, że {OPENAGHMATHJAX()}x^3>M{OPENAGHMATHJAX}. Podobnie dla dowolnie małego {OPENAGHMATHJAX()}m{OPENAGHMATHJAX} można wskazać takie {OPENAGHMATHJAX()}x{OPENAGHMATHJAX}, że {OPENAGHMATHJAX()}x^3<m{OPENAGHMATHJAX}.{OPENAGHEXAMPLE}

{OPENAGHDEFINITION( name="Funkcja ograniczona")}
__Funkcja__ {OPENAGHMATHJAX()}f:X\to F{OPENAGHMATHJAX} jest __ograniczona__, jeśli jest ona ograniczona zarówno z góry jak i z dołu.{OPENAGHDEFINITION}


{img type="fileId" fileId="566"}
 
{OPENAGHEXAMPLE( name="")}Funkcja {OPENAGHMATHJAX()}f(x)=2+\cos x{OPENAGHMATHJAX} jest ograniczona z góry przez liczbę {OPENAGHMATHJAX()}3{OPENAGHMATHJAX} i z dołu przez liczbę {OPENAGHMATHJAX()}1{OPENAGHMATHJAX}, czyli jest funkcją ograniczoną.{OPENAGHEXAMPLE}

{OPENAGHDEFINITION( name="Funkcja rosnąca")}
__Funkcja__ {OPENAGHMATHJAX()}f{OPENAGHMATHJAX} jest __rosnąca w zbiorze {OPENAGHMATHJAX()}A\subset D_f{OPENAGHMATHJAX}__, jeśli dla każdych dwóch elementów {OPENAGHMATHJAX()}x_1,x_2\in A{OPENAGHMATHJAX} stąd, że {OPENAGHMATHJAX()}x_1<x_2{OPENAGHMATHJAX} wynika, że {OPENAGHMATHJAX()}f(x_1)<f(x_2){OPENAGHMATHJAX}.{OPENAGHDEFINITION}

{img type="fileId" fileId="567"}
 

{OPENAGHDEFINITION( name="Funkcja słabo rosnąca")}
__Funkcja__ {OPENAGHMATHJAX()}f{OPENAGHMATHJAX} jest __słabo rosnąca (niemalejąca) w zbiorze {OPENAGHMATHJAX()}A\subset D_f{OPENAGHMATHJAX}__, jeśli dla każdych dwóch elementów {OPENAGHMATHJAX()}x_1,x_2\in A{OPENAGHMATHJAX} stąd, że {OPENAGHMATHJAX()}x_1<x_2{OPENAGHMATHJAX} wynika, że {OPENAGHMATHJAX()}f(x_1)\le f(x_2){OPENAGHMATHJAX}{OPENAGHDEFINITION}

{img type="fileId" fileId="568"}
 

{OPENAGHANNOTATION( name="")}Jak wynika z powyższych definicji, funkcja rosnąca to taka funkcja, dla której wraz ze wzrostem argumentu wzrasta wartość funkcji. Wykres funkcji rosnącej ,,wznosi się od lewej do prawej.{OPENAGHANNOTATION}

{OPENAGHDEFINITION( name="Funkcja malejąca")}
__Funkcja__ {OPENAGHMATHJAX()}f{OPENAGHMATHJAX} jest __malejąca w zbiorze {OPENAGHMATHJAX()}A\subset D_f{OPENAGHMATHJAX}__, jeśli dla każdych dwóch elementów {OPENAGHMATHJAX()}x_1,x_2\in A{OPENAGHMATHJAX} stąd, że {OPENAGHMATHJAX()}x_1<x_2{OPENAGHMATHJAX} wynika, że {OPENAGHMATHJAX()}f(x_1)>f(x_2){OPENAGHMATHJAX}{OPENAGHDEFINITION}


{img type="fileId" fileId="569"}


{OPENAGHDEFINITION( name="Funkcja słabo malejąca")}
__Funkcja__ {OPENAGHMATHJAX()}f{OPENAGHMATHJAX} jest __słabo malejąca (nierosnąca) w zbiorze {OPENAGHMATHJAX()}A\subset D_f{OPENAGHMATHJAX}__, jeśli dla każdych dwóch elementów {OPENAGHMATHJAX()}x_1,x_2\in A{OPENAGHMATHJAX} stąd, że {OPENAGHMATHJAX()}x_1<x_2{OPENAGHMATHJAX} wynika, że {OPENAGHMATHJAX()}f(x_1)\ge f(x_2){OPENAGHMATHJAX}{OPENAGHDEFINITION}

{img type="fileId" fileId="570"}

{OPENAGHANNOTATION( name="")}Funkcja malejąca to taka funkcja, dla której wraz ze wzrostem argumentu maleje wartość funkcji. Wykres funkcji malejącej ,,opada w dół od lewej do prawej.{OPENAGHANNOTATION}

{OPENAGHDEFINITION( name="Funkcja monotoniczna")}
__Funkcja monotoniczna w zbiorze {OPENAGHMATHJAX()}A\subset D_f{OPENAGHMATHJAX}__ to funkcja, która jest słabo rosnąca na {OPENAGHMATHJAX()}A{OPENAGHMATHJAX} lub słabo malejąca na {OPENAGHMATHJAX()}A{OPENAGHMATHJAX}.{OPENAGHDEFINITION}

__Funkcję__ nazywamy __ściśle monotoniczną w {OPENAGHMATHJAX()}A{OPENAGHMATHJAX}__, jeśli jest ona rosnąca lub malejąca.

{OPENAGHANNOTATION( name="")}Monotoniczność funkcji ma duże znaczenie podczas rozwiązywania nierówności.
Obrazowo można powiedzieć, że funkcje rosnące nie zmieniają zwrotu nierówności, natomiast funkcje malejące zmieniają ten zwrot.{OPENAGHANNOTATION}
Funkcje logartymiczne i wykładnicze o podstawie ułamkowej z przedziału {OPENAGHMATHJAX()}(0,1){OPENAGHMATHJAX} są malejące, stąd zmiana zwrotu podczas "opuszczania" symbolu tych funkcji, np:
Rozwiązując nierówność:

{OPENAGHMATHJAX()}\log_{1\over 2 }(3x+2)\le \log_{1\over 2 }x^2,{OPENAGHMATHJAX} 

pamiętamy, że funkcja {OPENAGHMATHJAX()}x\mapsto \log_{1\over 2}x{OPENAGHMATHJAX} jest malejąca i zmieniamy zwrot znaku nierówności przy ,,opuszczaniu logarytmu otrzymując nierówność kwadratową.
{OPENAGHMATHJAX()}3x+2\ge x^2,{OPENAGHMATHJAX}

Natomiast podczas rozwiązywania nierówności:
{OPENAGHMATHJAX()}\log_2(3x+2)\le \log_2x^2,{OPENAGHMATHJAX} wiedząc, że funkcja {OPENAGHMATHJAX()}x \mapsto log_2x{OPENAGHMATHJAX} jest rosnąca pozostawiamy niezmieniony zwrot nierówności otrzymując wówczas nierówność kwadratową
{OPENAGHMATHJAX()}3x+2\le x^2,{OPENAGHMATHJAX}

